<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        $anchuraDados=140;
        $alturaDados=140;
        
        ?>
        <style type="text/css">
            img{
                width: <?=$anchuraDados?>px;
                height: <?=$alturaDados?>px;
            }
            dados{
                width:<?=$anchuraDados*2?>px;
            }
            
            .total{
                margin:10px;
            }
            
            .total>span{
                border: black 2px solid; 
                padding: 10px; 
                font-size: 300%;
            }
        </style>
    </head>
    <body>
        <?php
        $tiradas=[];
        $sumaTiradas=[];
        function tirada(&$d1,&$d2){
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
        }
        
        for($c=0;$c<10;$c++){
            tirada($tiradas[$c][0],$tiradas[$c][1]);
            $sumaTiradas[]=$tiradas[$c][0]+$tiradas[$c][1];
        }
        
        foreach($tiradas as $i=>$v){
       
        ?>
        <div>
        <div class="dados">
            <img src="imgs/<?= $v[0] ?>.svg" alt="dado1"/>
            <img src="imgs/<?= $v[1] ?>.svg" alt="dado2"/>
        </div>
        <div class="total">Total: <span> <?= $sumaTiradas[$i] ?></span></div>
        </div>
        <?php
        }
        ?>
    </body>
</html>
