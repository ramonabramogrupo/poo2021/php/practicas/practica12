<?php
            $dado1= mt_rand(1,6);
            $dado2= mt_rand(1,6);
            $ancho="200px";
            $alto="200px";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .dados{
                width:<?= $ancho ?>;
                height:<?= $alto ?>;
            }

        </style>
    </head>
    <body>
        <?php
        require "./_cabecera.php";
        ?>
        <div style="width:<?= (int)$ancho*2 ?>px">
            <img class="dados" src="./imgs/<?= $dado1 ?>.svg">
            <img class="dados" src="./imgs/<?= $dado2 ?>.svg">
        </div>
        <div>
            Total <span style="padding:5px;border:1px solid black"><?= $dado1 + $dado2 ?></span>
        </div>
    </body>
</html>
